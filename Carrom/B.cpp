#include <iostream>
#include <cmath>
#include <GL/glut.h>
#include<math.h>
#include<string.h>
#include<stdio.h>

#define PI 3.141592653589
#define DEG2RAD(deg) (deg * PI / 180)

using namespace std;

void handleResize(int w, int h) 
{

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (float)w / (float)h, 0.1f, 200.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
class player
{
	public:
		int score;
		void set(int score_in)
		{
			score=score_in;
		}
		void update_score(int more_in)
		{
			score+=more_in;
		}
};

class hole
{
	public:

		float cur_x,cur_y,cur_z,radius;
		void set_part()
		{
			cur_x=0.0f;
			cur_y=0.0f;
			cur_z=0.0f;
		}
		void set(float radius_in)
		{
			radius=radius_in;
			set_part();
		}
		void translate(float x_in,float y_in,float z_in)
		{
			cur_x+=x_in;
			cur_y+=y_in;
			cur_z+=z_in;
			
		}
		void drawhole()
		{
			glPushMatrix();
			glTranslatef(cur_x,cur_y,cur_z);
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLE_FAN);
			for(int i=0 ; i<360 ; i++) 
			{
				glVertex2f(radius * cos(DEG2RAD(i)), radius * sin(DEG2RAD(i)));
			}
			glEnd();
			glPopMatrix();
		}
};

class box
{
	public:
		float box_len,r,g,b,cur_x,cur_y,cur_z;
		int type;
		void set_part()
		{
			cur_x=0.0f;
			cur_y=0.0f;
			cur_z=0.0f;
			
		}
		void set(float box_len_in,float r_in,float g_in,float b_in,float type_in=0 )
		{
			type=type_in;
			box_len=box_len_in;
			r=r_in;
			g=g_in;
			b=b_in;
			set_part();
		}
		void translate(float x_in,float y_in,float z_in)
		{
			cur_x+=x_in;
			cur_y+=y_in;
			cur_z+=z_in;
		}
		void drawbox()
		{
			glPushMatrix();
			glTranslatef(cur_x,cur_y,cur_z);
			glColor3f(r,g,b);
			float len=box_len;	
			if (type==1)
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			glBegin(GL_QUADS);
			glVertex2f(-len / 2, -len / 2);
			glVertex2f(len / 2, -len / 2);
			glVertex2f(len / 2, len / 2);
			glVertex2f(-len / 2, len / 2);
			glEnd();
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glPopMatrix();
		}
		
};
class coin
{
	public:
		float r,g,b,cur_x,cur_y,cur_z,radius,vel_x,vel_y,mass;
		int points,draw;
		void set_part()
		{
			cur_x=0.0f;
			cur_y=0.0f;
			cur_z=0.0f;
			vel_x=0.0f;
			vel_y=0.0f;
			draw=1;
			
		}
		void set(int points_in,float mass_in,float radius_in,float r_in,float g_in,float b_in)
		{		
			radius=radius_in;
			points=points_in;
			mass=mass_in;
			r=r_in;
			g=g_in;
			b=b_in;
			set_part();
		}
		void translate(float x_in,float y_in,float z_in)
		{
			cur_x+=x_in;
			cur_y+=y_in;
			cur_z+=z_in;
			
		}
		void set_velocity(float vel_x_in,float vel_y_in)
		{
				vel_x=vel_x_in;
				vel_y=vel_y_in;
		}
		void drawcoin()
		{
			int i=0;
			
			if(draw==1)
			{
				glPushMatrix();
				
				glTranslatef(cur_x,cur_y,cur_z);
				glColor3f(r,g,b);
				glBegin(GL_TRIANGLE_FAN);
				for(i=0 ; i<360 ; i++) 
				{
					glVertex2f(radius * cos(DEG2RAD(i)), radius * sin(DEG2RAD(i)));
				}
				glEnd();
				glPopMatrix();
			}
		}
};
box outer_rim,inner_rim;//,speed_box,fake_box;
hole hole_array[5];
coin white_coins[20],black_coins[20],striker,queen;
player player1;
int number_of_white_coins=4,number_of_black_coins=4,number_of_holes=4,pointer_allow=1,setter=0,try_num=1;
float temp[5];



class arrow
{
	public :
		float len,r,g,b,up,down;
		float x1,y1,x2,y2;
		int angle;
		void set_part()
		{
				r=0.0f;
				g=0.0f;
				b=0.0f;
				len=0.25f;
				up=0.5f;
				down=0.20f;
				angle=90;
		}
		void draw()
		{
	
			//cout << "THE ANGLE IS "<<  angle << endl;
			x1=striker.cur_x+(striker.radius*cos(angle*PI/180));
			y1=striker.cur_y+(striker.radius*sin(angle*PI/180));
				
			x2=striker.cur_x+((len + striker.radius)*cos(angle*PI/180));
			y2=striker.cur_y+((len + striker.radius)*sin(angle*PI/180));
			
			//cout << "drawing" << endl;
			glLineWidth(1.0f); // 
			glColor3f(r, g, b);
			glBegin(GL_LINES);
			glVertex3f(x1,y1,0.0f);
			glVertex3f(x2,y2,0.0f);
			glEnd();
		}
				
		
};
arrow pointer;

int condition_checker()
{
	// check if everything has stopped or not
	int i=1;
	for(i=1;i<=number_of_black_coins;i++)
	{
		if(black_coins[i].vel_x!=0.0f)
		{
			return 0;
		}
		if(black_coins[i].vel_y!=0.0f)
		{
			return 0;
		}
	}
	for(i=1;i<=number_of_white_coins;i++)
	{
		if(white_coins[i].vel_x!=0.0f)
		{
			return 0;
		}
		if(white_coins[i].vel_y!=0.0f)
		{
			return 0;
		}
	}
	if(queen.vel_x!=0.0f ) 
	{
		return 0;
	}
	if(queen.vel_y!=0.0f )
	{
		return 0;
	}
	if(striker.vel_x!=0.0f)
	{
		return 0;
	}
	if( striker.vel_y!=0.0f)
	{
		return 0;
	}
	return 1;
}
void handler1()
{
	int saver=-1;
	//float len=outer_rim.box_len;
	
	if(striker.draw==0)
	{
		saver=condition_checker();
		if(saver==1)
		{
			striker.set(0,1.0f,0.088f,0.36f,0.56f,0.96f);
			striker.translate(0.0f,-1.405f,0.0f);
			striker.set_velocity(0.0f,0.0f);
			cout << "handler1" << endl;
			pointer.set_part();
			pointer_allow=1;
		}
	}
}
void handler2()
{	
	int saver=-1;
	//	float len=outer_rim.box_len;
	
	if(pointer_allow==0 && striker.vel_x==0.0f && striker.vel_y==0.0f)
	{
		saver=condition_checker();
		if(saver==1)
		{
			striker.set(0,1.0f,0.088f,0.36f,0.56f,0.96f);
			striker.translate(0.0f,-1.405f,0.0f);
			striker.set_velocity(0.0f,0.0f);
			cout << "handler2" << endl;			
			pointer.set_part();
			pointer_allow=1;
		}
	}
	
	if(pointer_allow==1)
	{
		//cout << "THE ANGLE IS "<<  pointer.angle << endl;
		pointer.draw();
	}
	striker.drawcoin();	
}
coin cir1,cir2,cir3,cir4,cir5,cir7,cir8;


// Initializing some openGL 3D rendering options
void initRendering() 
{
	glEnable(GL_DEPTH_TEST);        // Enable objects to be drawn ahead/behind one another
	glEnable(GL_COLOR_MATERIAL);    // Enable colozring
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);   // Setting a background color
}


void draw_design(float depth)
{
	//int i=1,len=outer_rim.box_len,div=5,below=-1.0f;
	
	int i=1;
	for(i=1;i<=4;i++)
	{
		
		//cout << "hello";
		glLineWidth(3.0f); // Line Below
		glColor3f(0.0, 0.0, 0.0);
		
		glBegin(GL_LINES);
		glVertex3f(-1.3f, -1.3f,0.0);
		glVertex3f(1.3f,-1.3f, 0.0);
		glEnd();

		glLineWidth(3.1f); // Line Above
		glColor3f(0.0, 0.0, 0.0);
		
		glBegin(GL_LINES);
		glVertex3f(-1.3f, -1.5f, 0.0);
		glVertex3f(1.3f, -1.5f, 0.0);
		glEnd();
		
		glPushMatrix(); // Position Circle 1
		
		glPushMatrix();
		cir7.set(0,0,0.09f,0.0f,0.0f,0.0f);
		cir7.translate(-1.27f,-1.4f,0.0f);
		cir7.drawcoin();
		glPopMatrix();
		
		glPushMatrix();
		cir1.set(0,0,0.07f,1.0f,0.0f,0.0f);
		cir1.translate(-1.27f,-1.4f,0.0f);
		cir1.drawcoin();
		glPopMatrix();
		
		
		glPushMatrix();
		cir8.set(0,0,0.09f,0.0f,0.0f,0.0f);
		cir8.translate(1.27f,-1.4f,0.0f);
		cir8.drawcoin();
		glPopMatrix();
		
		
		glPushMatrix(); // Position Circle 2
		cir2.set(0,0,0.07f,1.0f,0.0f,0.0f);
		cir2.translate(1.27f,-1.4,0.0f);
		cir2.drawcoin();
		glPopMatrix();
		
		glLineWidth(2.5f); // Line Above
		glColor3f(0.0, 0.0, 0.0);
		
		
		glBegin(GL_LINES);
		glVertex3f(-1.45f, -1.45f,0.0);
		glVertex3f(-0.7,-0.7,0);
		glEnd();
		
		// triangle
		glLineWidth(2.2f);  
		glBegin(GL_LINES);
		glVertex3f(-0.8,-0.7,0.0);
		glVertex3f(-0.7,-0.7,0);
		glEnd();
		
		glLineWidth(2.2f); 
		glBegin(GL_LINES);
		glVertex3f(-0.7f, -0.7f,0.0);
		glVertex3f(-0.7,-0.82,0);
		glEnd();
		glPopMatrix();
		
		glRotatef(90,0.0f,0.0f,depth);
	} 
	
	cir3.set(0,0.0,0.45f,1.0f,0.0f,0.0f);
	cir3.drawcoin();
	
	cir4.set(0,0.0,0.40f,inner_rim.r,inner_rim.g,inner_rim.b);
	cir4.drawcoin();
	
	cir5.set(0,0.0,0.075f,1.0f,0.0f,0.0f);
	cir5.drawcoin();
}
void in_out(int depth)
{
	float len=4.0f;
	
	glTranslatef(0.0f,0.0f,depth);
	//cout << len << endl;
	
	outer_rim.set(len,0.54f,0.22f,0.06f,1);
	outer_rim.drawbox();
	
	inner_rim.set(0.9*len,0.95f,0.90f,0.70f,1);
	inner_rim.drawbox();
}
void hole_set()
{
	float len=outer_rim.box_len,a=1.612f,factor=0.045f;
		
	glPushMatrix();
	hole_array[1].set(factor*len);
	hole_array[1].translate(a,a,0.0f);
	hole_array[1].drawhole();
	glPopMatrix();

	glPushMatrix();
	hole_array[2].set(factor*len);
	hole_array[2].translate(-1*a,a,0.0f);
	hole_array[2].drawhole();
	glPopMatrix();
	
	glPushMatrix();
	hole_array[3].set(factor*len);
	hole_array[3].translate(a,-1*a,0.0f);
	hole_array[3].drawhole();
	glPopMatrix();
	
	glPushMatrix();
	hole_array[4].set(factor*len);
	hole_array[4].translate(-1*a,-1*a,0.0f);
	hole_array[4].drawhole();
	glPopMatrix();
}

void coin_set()
{
	int i=1,angle=0,j=1;
	float len=outer_rim.box_len,rad=0.285f;//,a=-1.323f;
	
	number_of_white_coins=number_of_black_coins;
	
	
	while(i<=number_of_white_coins)
	{
		glPushMatrix();
		
		if(setter==0)
		{
			black_coins[i].set(10,1.0f,0.025*len,0.8f,0.8f,0.8f);
			black_coins[i].translate(rad*cos(angle*PI/180),rad*sin(angle*PI/180),0.0f);
			black_coins[i].set_velocity(0.0f,0.0f);
		}
		
		black_coins[i].drawcoin();
		glPopMatrix();
		
		i++;
		angle+=45;
		
		glPushMatrix();
		
		if(setter==0)
		{
			white_coins[j].set(20,1.0f,0.025*len,0.3f,0.3f,0.3f);
			white_coins[j].translate(rad*cos(angle*PI/180),rad*sin(angle*PI/180),0.0f);
			white_coins[j].set_velocity(0.0f,0.0f);
		}
	
		white_coins[j].drawcoin();
		glPopMatrix();
		
		j++;
		angle+=45;
	}	
	
	glPushMatrix();
	if(setter==0)
	{
		queen.set(0,1.0f,0.025*len,0.7f,0.055f,0.45f);
		queen.translate(0.0f,0.0f,0.0f);
		queen.set_velocity(0.00f,0.0f);
	}
	queen.drawcoin();
	glPopMatrix();
	
	setter=1;
	
}

int coin_reset(coin* a,int mode)
{
	
	a->cur_x=0.0f;
	a->cur_y=0.0f;
	a->cur_z=0.0f;
	a->vel_x=0.0f;
	a->vel_y=0.0f;
	a->draw=0;
	
	
	if(mode==1)
	{
		striker.draw=0;
			
	}
	
	
	return 0;
}

void collision_manager0(hole hole_in,coin* coin_in,int type_in)
{
	
	float a=hole_in.radius,b=coin_in->radius;
	float c=hole_in.cur_x,d=hole_in.cur_y,e=hole_in.cur_z;
	float f=coin_in->cur_x,g=coin_in->cur_y,h=coin_in->cur_z;
	
	float i=f-c,j=g-d,k=h-e;
	float l=(i*i) + (j*j) + (k*k) ;
	
	
	//cout << striker.draw << endl;
	if(a+b>sqrt(l)+0.5 && coin_in->draw==1) 
	{
		player1.update_score(coin_in->points);
		cout << player1.score << endl;
		coin_reset(coin_in,type_in);
		
	}
	
}

int collision_manager1(coin* a,coin* b)
{
	float m=a->radius,n=b->radius;
	float c=a->cur_x,d=a->cur_y,e=a->cur_z;
	float f=b->cur_x,g=b->cur_y,h=b->cur_z;
		
	float i=f-c,j=g-d,k=h-e;
	float l=(i*i) + (j*j) + (k*k) ;//float diminish=hole_in.radius*0.035f
	
	float meta;
	float theta;
	
	theta=((a->cur_y-b->cur_y)/(a->cur_x - b->cur_x)+0.00001);
	meta =atan(theta);
	
	float temp;
	// unresolved
	float avelx=a->vel_x;
	float avely=a->vel_y;
	float bvelx=b->vel_x;
	float bvely=b->vel_y;
	
	float t1=2*(b->cur_x-a->cur_x)*(bvelx-avelx+0.00001);
	
	float t2=2*(b->cur_y-a->cur_y)*(bvely-avely+0.00001);
	
	//cout << "welcome" << endl;
	if(a->draw==1 && b->draw==1)
	{
		
		if(m+n>sqrt(l)) 
		{
			if(t1+t2<0)
			{
			//cout << avelx << "--------"<< avely << "||"<< bvelx << "--------"<< bvely<< endl;
			// resolving coin a  
			
				float avelx_dash=avelx*cos(meta) + avely*sin(meta); 
				float avely_dash=avely*cos(meta) - avelx*sin(meta);
				
				// resolving coin b
				
				float bvelx_dash=bvelx*cos(meta) + bvely*sin(meta); 
				float bvely_dash=bvely*cos(meta) - bvelx*sin(meta);
				
				// swapping 
				
				temp=avelx_dash;
				avelx_dash=bvelx_dash;
				bvelx_dash=temp;
				
				// re resolving coin a
				
				a->vel_x=avelx_dash*cos(meta) - avely_dash*sin(meta); 
				a->vel_y=avely_dash*cos(meta) + avelx_dash*sin(meta);
				
				// re resolving coin b
				
				b->vel_x=bvelx_dash*cos(meta) - bvely_dash*sin(meta); 
				b->vel_y=bvely_dash*cos(meta) + bvelx_dash*sin(meta);
				
				return 101;
			}
		}
	}
	return -101;
}


void collision_manager2(coin *a)
{
	//done
	if(a->cur_x + a->radius >inner_rim.box_len/2 || a->cur_x - a->radius < -1*inner_rim.box_len/2)
    {
		a->vel_x *= -1;
	}
    if(a->cur_y + a->radius >inner_rim.box_len/2 || a->cur_y - a->radius < -1*inner_rim.box_len/2)
    {
		a->vel_y *= -1;
	}
}

void manager()
{
	int i,k,j,l;
	
	for(i=1;i<=number_of_holes;i++)
	{
			for(j=1;j<=number_of_black_coins;j++)
			{
				collision_manager0(hole_array[i],&black_coins[j],0);
			}
			for(k=1;k<=number_of_white_coins;k++)
			{
				collision_manager0(hole_array[i],&white_coins[k],0);
			}
			collision_manager0(hole_array[i],&striker,1);
			collision_manager0(hole_array[i],&queen,2);
	}

	for(i=1;i<=number_of_black_coins;i++)
	{
		for(l=1;l<=number_of_white_coins;l++)
		{
			collision_manager1(&black_coins[i],&white_coins[l]);
		}
		collision_manager1(&black_coins[i],&striker);
		collision_manager1(&black_coins[i],&queen);
	}
	
	for(i=1;i<=number_of_white_coins;i++)
	{
		collision_manager1(&white_coins[i],&striker);
		collision_manager1(&white_coins[i],&queen);
	}
	collision_manager1(&striker,&queen);
	
	for(i=1;i<=number_of_black_coins;i++)
	{
		collision_manager2(&black_coins[i]);
	}	
	for(i=1;i<=number_of_white_coins;i++)
	{
		collision_manager2(&white_coins[i]);
	}
	collision_manager2(&striker);
	collision_manager2(&queen);
}
void position_updater(coin *a)
{
	float dec=0.0005f;
	if(fabs(a->vel_x)<dec)
	{
		a->vel_x=0.0f;
	}
	else
	{
		if(a->vel_x>0)
		{
			a->vel_x -=dec;
		}
		else if(a->vel_x<0)
		{
			a->vel_x +=dec;
		}
	}
	if(fabs(a->vel_y)<dec)
	{
		a->vel_y=0.0f;
	}
	else
	{
		if(a->vel_y>0)
		{
			a->vel_y -=dec;
		}
		else if(a->vel_y<0)
		{
			a->vel_y+=dec;
		}
	}
	a->cur_x+=a->vel_x;
	a->cur_y+=a->vel_y;
}

void update(int value)
{
    int i=1;
    for(i=1;i<=number_of_black_coins;i++)
    {
		position_updater(&black_coins[i]);
	}
	for(i=1;i<=number_of_white_coins;i++)
	{
		position_updater(&white_coins[i]);
	}
	position_updater(&queen);
	position_updater(&striker);
	
	manager();
    glutTimerFunc(1, update, 0);
}

void handleKeypress1(unsigned char key, int x, int y) 
{
    if (key == 27) 
    {
        exit(0);     // escape key is pressed
    }
    if(key=='A')
    {
		pointer.angle += 5/4;
		cout << "hello" << pointer.angle<< endl;
		//exit(0);
	}
	if(key=='D')
    {
		
		pointer.angle -= 5/4;
		//cout << "hello" << pointer.angle<< endl;
		//exit(0);
	}
	
	if(key==13 && striker.vel_x==0.0f && striker.vel_y==0.0f)
    {
		striker.vel_x=pointer.len*cos(pointer.angle*PI/180)/6;
		striker.vel_y=pointer.len*sin(pointer.angle*PI/180)/6;
		pointer_allow=0;
		cout << try_num << endl;
		try_num++;
	}     
}

void handleKeypress2(int key, int x, int y) 
{
	float increment1=0.020f,increment2=0.05f;
	
	if (key == GLUT_KEY_LEFT && striker.cur_x-increment2>=-1.322f && striker.vel_x==0.0f && striker.vel_y==0.0f)
	{
		//cout << striker.cur_x << endl;
        striker.cur_x -= increment1;
		pointer.x1 -=increment1;
		pointer.x2 -=increment1;
		//cout << striker.cur_x << endl;
		
    }
    if (key == GLUT_KEY_RIGHT && striker.cur_x+increment2<=1.322f && striker.vel_x==0.0f && striker.vel_y==0.0f)
    {
        striker.cur_x += increment1;
        pointer.x1 +=increment1;
		pointer.x2 +=increment1;
    }
    
    if (key == GLUT_KEY_UP && pointer.len<=pointer.up )
    {
		//cout << "start " << pointer.len << endl;
		pointer.len += increment1/2;
		//cout << "end " << pointer.len << endl;
	} 
    if (key == GLUT_KEY_DOWN && pointer.len>=pointer.down)
    {
        pointer.len -= increment1/2;
    }
}
void handleMouseclick(int button, int state, int x, int y) 
{

    if (state == GLUT_DOWN)
    {
        if (button == GLUT_LEFT_BUTTON)
        {
            pointer.angle += 15;
            //cout << "left plus "<< pointer.angle<< endl;
        }
        else if (button == GLUT_RIGHT_BUTTON)
        {
			pointer.angle -= 15;
			//cout << pointer.angle;
		}
    }
}
void print_score(float depth)
{
	char ch[8] = "Score: ";
	float len=inner_rim.box_len;
	glPushMatrix();
    
    glColor3f(0.0f,0.0f,0.0f);
	glTranslatef(0,0,depth);
	
    glRasterPos2f(0-0.7f, len/2+1.4f);
    int string_len, i;
    string_len = (int)strlen(ch);
    for (i = 0; i < string_len; i++) 
    {
            glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ch[i]);
    }
    char b[10];
    snprintf(b, sizeof(b), "%d", player1.score);
    string_len = (int)strlen(b);
    
    //printf("%s",b);
    //exit(0);   
    glRasterPos2f(0.6, len/2 + 1.4f);
    for (i = 0; i < string_len; i++)
    {
         glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, b[i]);
    }
    
    glPopMatrix();
}
void exit_checker()
{
	int i=1;
	for(i=1;i<=number_of_black_coins;i++)
	{
		if(black_coins[i].draw!=0)
		{
			return ;
		}
	}
	for(i=1;i<=number_of_white_coins;i++)
	{
		if(white_coins[i].draw!=0)
		{
			return ;
		}
	}
	
	exit(0);
	
}
void dec_score(int a)
{
	player1.score--;
	glutTimerFunc(1000, dec_score,0 );
	
}

void drawscene()
{
	
	float depth=-5.0f;
	float saver=-1;
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();

	in_out(depth);
	draw_design(depth);
	hole_set();
	handler1();
	handler2();
	coin_set();
	
	saver=condition_checker();
	if(saver==1)
	{
		print_score(depth);
		saver=-1;
	}
	
	exit_checker();
	
	
	glPopMatrix();
	glutSwapBuffers();
}

int main(int argc,char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	int w = glutGet(GLUT_SCREEN_WIDTH);
	int h = glutGet(GLUT_SCREEN_HEIGHT);

	int windowWidth = w * 2 / 3;
	int windowHeight = h * 2 / 3;

	player1.set(0);
	
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition((w - windowWidth) / 2, (h - windowHeight) / 2);

	glutCreateWindow("Collision detection ");  // Setup the window
	initRendering();
	
	
	glutKeyboardFunc(handleKeypress1);
    glutSpecialFunc(handleKeypress2);
	glutDisplayFunc(drawscene);
	glutIdleFunc(drawscene);
	glutReshapeFunc(handleResize);
	glutMouseFunc(handleMouseclick);
	
	glutTimerFunc(1, update, 0);
	glutTimerFunc(1000,dec_score,0);
	glutMainLoop();
}	
